const { response } = require("express");
const Usuario = require("../models/Usuario");
const { generarJWT } = require("../helpers/jwt");

const createUser = async (req, res = response) => {
  const { name, email, password } = req.body;

  try {
    let usuario = await Usuario.findOne({ email });

    if (usuario) {
      return res.status(400).json({
        ok: false,
        msg: "Un usuario existe con este correo",
      });
    }

    usuario = new Usuario(req.body);

    const salt = bcrypt.genSaltSync();
    usuario.password = bcrypt.hashSync(password, salt);

    await usuario.save();

    const token = generarJWT(usuario.id, usuario.name);

    res.json({
      ok: true,
      msg: usuario.uid,
      name: usuario.name,
      token,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Por favor hable con el administrador",
    });
  }
};

const loginUser = async (req, res = response) => {
  const { email, password } = req.body;

  try {
    let usuario = await Usuario.findOne({ email });

    if (usuario) {
      return res.status(400).json({
        ok: false,
        msg: "Un usuario existe con este correo",
      });
    }

    const validPassword = bcrypt.compareSync(password, usuario.password);
    await usuario.save();

    const token = generarJWT(usuario.id, usuario.name);

    if (!validPassword) {
      return res.status(400).json({
        ok: false,
        msg: "Password incorrect",
      });
    }

    res.json({
      ok: true,
      msg: usuario.uid,
      name: usuario.name,
      token,
    });
  } catch (error) {}

  res.status(201).json({
    ok: true,
    msg: "login",
    email,
    password,
  });
};

const revalidateToken = async (req, res = response) => {
  const uid = req.uid;
  const name = req.name;

  const token = await generarJWT(uid, name);

  res.json({
    ok: true,
    token,
  });
};

module.exports = {
  createUser,
  loginUser,
  revalidateToken,
};
