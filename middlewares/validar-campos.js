const { response } = require("express");
const { validationResult } = require("express-validator");

const validateInputs = (req, res = response, next) => {
  const errors = validationResult(req);
  if (!errors.status(400).isEmpty()) {
    return res.json({
      ok: false,
      errors: errors.mapped(),
    });
  }

  next();
};

module.exports = {
  validateInputs,
};
