const { Schema, model } = require("mongoose");

const EventosSchema = Schema({
  title: {
    type: String,
    required: true,
  },
  notes: {
    type: String,
  },
  start: {
    type: Date,
    required: true,
  },
  end: {
    type: Date,
    required: true,
  },
  end: {
    type: Schema.Types.ObjectId,
  },
});

EventosSchema.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

module.exports = model("Eventos", EventosSchema);
