const { Router } = require("express");
const {
  createUser,
  revalidateToken,
  loginUser,
} = require("../controllers/auth");
const router = Router();
const { check } = require("express-validator");
const { validarCampos } = require("../middlewares/validar-campos");
const { validarJWT } = require("../middlewares/validar-jwt");
router.post(
  "/new",
  [
    check("name", "El nombre es obligatorio").not().isEmpty(),
    check("email", "El email es obligatorio").isEmail(),
    check("password", "El password debe tener seis caracteres").isLength({
      min: 6,
    }),
    validarCampos,
  ],
  createUser
);

router.post(
  "/",
  [
    check("email", "El email es obligatorio").isEmail(),
    check("password", "El password debe tener seis caracteres").isLength({
      min: 6,
    }),
    validarCampos,
  ],
  loginUser
);

router.get("/renew", validarJWT, revalidateToken);

module.exports = router;
