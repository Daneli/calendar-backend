const { Router } = require("express");
const { validarJWT } = require("../middlewares/validar-jwt");
const {
  getEvento,
  crearEvento,
  actualizarEvento,
  eliminarEvento,
} = require("../controllers/events");
const router = Router();
const { check } = require("express-validator");
const { validarCampos } = require("../middlewares/validar-campos");
const { isDate } = require("../helpers/isDate");

router.get("/", validarJWT, getEvento);
router.post(
  "/",
  validarJWT,
  [
    check("title", "El titulo es obligatorio").not().isEmpty(),
    check("start", "Fecha de inicio es obligatoria").custom(isDate),
    validarCampos,
  ],
  crearEvento
);
router.post("/:id", validarJWT, actualizarEvento);
router.put("/:id", validarJWT, eliminarEvento);

module.exports = router;
